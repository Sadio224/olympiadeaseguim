create database olympiades;
use olympiades;

-- ------------------------------------------------------------
-- 1E: Création des tables
-- ------------------------------------------------------------
drop table users cascade constraint purge;
create table users(
					username_user varchar(50),
					password_user varchar(50)
						
				 );

--question et réponses de maths
drop table question_maths cascade constraint purge;
create table question_maths(
							id_question_maths int,
							contenu_question_maths text
							);

drop table reponse_maths cascade constraint purge;
create table reponse_maths(
							id_reponse_maths int,
							contenu_reponse_maths text,
							bingo_reponse_maths tinyint(1),
							id_question_maths int
							);


--question et réponses de biologie
drop table question_biologie cascade constraint purge;
create table question_biologie(
								id_question_biologie int,
								contenu_question_biologie text
							  );

drop table reponse_biologie cascade constraint purge;
create table reponse_biologie(
							    id_reponse_biologie int,
							    contenu_reponse_biologie text,
							    bingo_reponse_biologie tinyint(1),
							    id_question_biologie int
							);


--question et réponses de science_sociale
drop table question_science_sociale cascade constraint purge;
create table question_science_sociale(
									  id_question_science_sociale int,
									  contenu_question_science_sociale text
									);


drop table reponse_science_sociale cascade constraint purge;
create table reponse_science_sociale(
									  id_reponse_science_sociale int,
									  contenu_reponse_science_sociale text,
									  bingo_reponse_science_sociale tinyint(1),
									  id_question_science_sociale int
									);



--question et réponses de ntic
drop table question_ntic cascade constraint purge;
create table question_ntic(
							id_question_ntic int,
							contenu_question_ntic text
							);


drop table reponse_ntic cascade constraint purge;
create table reponse_ntic(
					  id_reponse_ntic int,
					  contenu_reponse_ntic text,
					  bingo_reponse_ntic tinyint(1),
					  id_question_ntic int
					);
-- ------------------------------------------------------------
-- 3E: Ajout des contraintes: clef primaire, clef secondaire,...
-- ------------------------------------------------------------

-- ajout des clefs primaires
ALTER TABLE question_maths
ADD CONSTRAINT pk_id_question_maths PRIMARY KEY (id_question_maths);

ALTER TABLE question_biologie
ADD CONSTRAINT pk_id_question_biologie PRIMARY KEY (id_question_biologie);

ALTER TABLE question_science_sociale
ADD CONSTRAINT pk_id_question_science_sociale PRIMARY KEY (id_question_science_sociale);

ALTER TABLE question_ntic
ADD CONSTRAINT pk_id_question_ntic PRIMARY KEY (id_question_ntic);





ALTER TABLE reponse_maths
ADD CONSTRAINT pk_id_reponse_maths PRIMARY KEY (id_reponse_maths);

ALTER TABLE reponse_biologie
ADD CONSTRAINT pk_id_reponse_biologie PRIMARY KEY (id_reponse_biologie);

ALTER TABLE reponse_science_sociale
ADD CONSTRAINT pk_id_reponse_science_sociale PRIMARY KEY (id_reponse_science_sociale);

ALTER TABLE reponse_ntic
ADD CONSTRAINT pk_id_reponse_ntic PRIMARY KEY (id_reponse_ntic);






-- ajout de la contrainte auto_increment
ALTER TABLE question_maths MODIFY id_question_maths INT NOT NULL AUTO_INCREMENT;
ALTER TABLE question_biologie MODIFY id_question_biologie INT NOT NULL AUTO_INCREMENT;

ALTER TABLE question_science_sociale MODIFY id_question_science_sociale INT NOT NULL AUTO_INCREMENT;
ALTER TABLE question_ntic MODIFY id_question_ntic INT NOT NULL AUTO_INCREMENT;

ALTER TABLE reponse_maths MODIFY id_reponse_maths INT NOT NULL AUTO_INCREMENT;
ALTER TABLE reponse_biologie MODIFY id_reponse_biologie INT NOT NULL AUTO_INCREMENT;

ALTER TABLE reponse_science_sociale MODIFY id_reponse_science_sociale INT NOT NULL AUTO_INCREMENT;
ALTER TABLE reponse_ntic MODIFY id_reponse_ntic INT NOT NULL AUTO_INCREMENT;





-- ajout des clef secondaires
ALTER TABLE reponse_maths
ADD CONSTRAINT fk_id_question_maths FOREIGN KEY (id_question_maths) REFERENCES question_maths(id_question_maths);


ALTER TABLE reponse_biologie
ADD CONSTRAINT fk_id_question_biologie FOREIGN KEY (id_question_biologie) REFERENCES question_maths(id_question_biologie);


ALTER TABLE reponse_science_sociale 
ADD CONSTRAINT fk_id_question_science_sociale FOREIGN KEY (id_question_science_sociale) REFERENCES question_maths(id_question_science_sociale);


ALTER TABLE reponse_ntic 
ADD CONSTRAINT fk_id_question_ntic FOREIGN KEY (id_question_ntic) REFERENCES question_ntic(id_question_ntic);




-- ------------------------------------------------------------
-- insertion de données dans la base de données (remplissage)
-- ------------------------------------------------------------
--Remarque:  au départ on insère des données vide dans les tables pour éviter que le système ne crash quand il ne trouvera aucune entrée
--insetion des questions et reponses de Maths
INSERT INTO question_maths 
VALUES (null, '');

-- les reponses de la question 1 de maths
INSERT INTO reponse_maths 
VALUES (null, '', 0, 1) ;




--insetion des questions et reponses de Biologie
INSERT INTO question_biologie 
VALUES (null, '');


-- les reponses de la question 1 de Biologie
INSERT INTO reponse_biologie 
VALUES (null, '', 0, 1) ;




--insetion des questions et reponses de Science sociale

INSERT INTO question_science_sociale 
VALUES (null, '');



-- les reponses de la question 1 de de Science sociale
INSERT INTO reponse_science_sociale
VALUES (null, '', 0, 1) ;





--insetion des questions et reponses de ntic

INSERT INTO question_ntic
VALUES (null, '');

-- les reponses de la question 1 de de ntic
INSERT INTO reponse_ntic 
VALUES (null, '', 0, 1) ;

















create table users(
					id_user int,
					username_user varchar(50),
					password_user varchar(50)
						
				 );

ALTER TABLE users
ADD CONSTRAINT pk_id PRIMARY KEY (id_user);


ALTER TABLE users MODIFY id_user INT AUTO_INCREMENT;


INSERT INTO users values (null, "aseguim", "aseguim"),(null, "olympiades", "olympiades"), (null, "nanamou", "nanamou"); 