package olympiades;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.*;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.Timer;
/**
 *
 * @Nanamou, 1er projet d'apprentissage sous Netbeans
 */
public class ProgressBarDemo extends javax.swing.JFrame implements CustomPanel.ProgressBarInterface 
{
    public static int valeur=0;
    public static boolean etat=true;
    
    //Creates new form ProgressBarDemo
    public ProgressBarDemo() 
    {
        initComponents();
        this.setLocationRelativeTo(null);
        jp_progress.pbr = this;
        
        //par défaut, les conteneurs de question et reponse ne seront pas visibles
        question.setVisible(true);
        reponse_panel.setVisible(false);
        bt_run.setVisible(true);       
        jButton2.setVisible(false);
        label_erreur.setVisible(true);
        jPanel4.setVisible(false); 
        labelChrono.setVisible(false);
        jLabel4.setVisible(false);
        jLabel21.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153,0,0),5));
        jLabel13.setText("Mathématiques");
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        principal_bleu = new javax.swing.JPanel();
        parametre = new javax.swing.JPanel();
        corps = new javax.swing.JPanel();
        jPanel1 = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        scoreA = new javax.swing.JLabel();
        scoreB = new javax.swing.JLabel();
        labelEquipeB = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        jLabel19 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        jp_progress = new olympiades.CustomPanel();
        jPanel3 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        question = new javax.swing.JTextArea();
        reponse_panel = new javax.swing.JPanel();
        reponse0 = new javax.swing.JLabel();
        reponse2 = new javax.swing.JLabel();
        reponse1 = new javax.swing.JLabel();
        reponse3 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        jPanel5 = new javax.swing.JPanel();
        jLabel6 = new javax.swing.JLabel();
        jLabel21 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel23 = new javax.swing.JLabel();
        labelEquipeA = new javax.swing.JLabel();
        retrait_joueur1 = new javax.swing.JLabel();
        ajout_joueur1 = new javax.swing.JLabel();
        ajout_joueur2 = new javax.swing.JLabel();
        retrait_joueur2 = new javax.swing.JLabel();
        label_erreur = new javax.swing.JLabel();
        retrait_joueur3 = new javax.swing.JLabel();
        ajout_joueur3 = new javax.swing.JLabel();
        scoreC = new javax.swing.JLabel();
        labelEquipeC = new javax.swing.JLabel();
        menu = new javax.swing.JPanel();
        jPanel4 = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        labeEquipeB_in = new javax.swing.JTextField();
        labeEquipeA_in = new javax.swing.JTextField();
        labelChrono = new javax.swing.JTextField();
        jLabel27 = new javax.swing.JLabel();
        labelErreur = new javax.swing.JTextField();
        jSeparator1 = new javax.swing.JSeparator();
        jSeparator4 = new javax.swing.JSeparator();
        labeEquipeC_in = new javax.swing.JTextField();
        jLabel10 = new javax.swing.JLabel();
        jSeparator2 = new javax.swing.JSeparator();
        jLabel16 = new javax.swing.JLabel();
        jLabel17 = new javax.swing.JLabel();
        jSeparator3 = new javax.swing.JSeparator();
        jLabel28 = new javax.swing.JLabel();
        jLabel22 = new javax.swing.JLabel();
        jLabel20 = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        bt_run = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setLocationByPlatform(true);
        setUndecorated(true);
        setResizable(false);
        setSize(new java.awt.Dimension(1590, 950));

        principal_bleu.setBackground(new java.awt.Color(51, 51, 255));
        principal_bleu.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        parametre.setBackground(new java.awt.Color(255, 255, 0));
        parametre.setMaximumSize(new java.awt.Dimension(1590, 950));
        parametre.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        corps.setBackground(new java.awt.Color(255, 255, 255));
        corps.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 0, 0)));
        corps.setMaximumSize(new java.awt.Dimension(1590, 950));
        corps.setMinimumSize(new java.awt.Dimension(1590, 950));
        corps.setPreferredSize(new java.awt.Dimension(1590, 950));
        corps.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel1.setBackground(new java.awt.Color(153, 0, 0));

        jLabel3.setFont(new java.awt.Font("Verdana", 1, 12)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(255, 255, 255));
        jLabel3.setText("Copyright © XYZ.Society for Olympiades ASEGUIM 2019 | Tous droits réservés  | Une application de XYZ.Society");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(260, 260, 260)
                .addComponent(jLabel3)
                .addContainerGap(271, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        corps.add(jPanel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 910, 1280, 40));

        scoreA.setFont(new java.awt.Font("Agency FB", 1, 120)); // NOI18N
        scoreA.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        scoreA.setText("0");
        scoreA.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        corps.add(scoreA, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 750, 210, 100));

        scoreB.setFont(new java.awt.Font("Agency FB", 1, 120)); // NOI18N
        scoreB.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        scoreB.setText("0");
        scoreB.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        corps.add(scoreB, new org.netbeans.lib.awtextra.AbsoluteConstraints(480, 750, 220, 100));

        labelEquipeB.setFont(new java.awt.Font("Agency FB", 1, 36)); // NOI18N
        labelEquipeB.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        labelEquipeB.setText("Equipe B");
        labelEquipeB.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        corps.add(labelEquipeB, new org.netbeans.lib.awtextra.AbsoluteConstraints(420, 700, 340, 47));

        jLabel5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/logoASEGUIM.png"))); // NOI18N
        jLabel5.setAlignmentX(20.0F);
        jLabel5.setAlignmentY(5.0F);
        jLabel5.setAutoscrolls(true);
        jLabel5.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        jLabel5.setRequestFocusEnabled(false);
        corps.add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(1, 40, 1277, -1));

        jPanel2.setBackground(new java.awt.Color(153, 0, 0));
        jPanel2.setCursor(new java.awt.Cursor(java.awt.Cursor.MOVE_CURSOR));
        jPanel2.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseDragged(java.awt.event.MouseEvent evt) {
                jPanel2MouseDragged(evt);
            }
        });
        jPanel2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jPanel2MousePressed(evt);
            }
        });

        jLabel19.setFont(new java.awt.Font("Verdana", 1, 15)); // NOI18N
        jLabel19.setForeground(new java.awt.Color(255, 255, 255));
        jLabel19.setText("Olympiades ASEGUIM 2019");

        jLabel11.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/Minus_33px_2.png"))); // NOI18N
        jLabel11.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jLabel11.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jLabel11.setFocusable(false);
        jLabel11.setRequestFocusEnabled(false);
        jLabel11.setVerifyInputWhenFocusTarget(false);
        jLabel11.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel11MouseClicked(evt);
            }
        });

        jLabel12.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel12.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/Unavailable_29px_1.png"))); // NOI18N
        jLabel12.setAlignmentX(5.0F);
        jLabel12.setAlignmentY(5.0F);
        jLabel12.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jLabel12.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jLabel12.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel12MouseClicked(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap(891, Short.MAX_VALUE)
                .addComponent(jLabel19, javax.swing.GroupLayout.PREFERRED_SIZE, 251, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(376, 376, 376)
                .addComponent(jLabel11, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(jLabel12, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel11, javax.swing.GroupLayout.DEFAULT_SIZE, 40, Short.MAX_VALUE)
            .addComponent(jLabel12, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jLabel19, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        corps.add(jPanel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(-320, 0, 1600, 40));

        jp_progress.setForeground(java.awt.Color.red);
        jp_progress.setToolTipText("");
        jp_progress.setDoubleBuffered(false);
        jp_progress.setEnabled(false);
        jp_progress.setFocusable(false);
        jp_progress.setOpaque(false);
        jp_progress.setRequestFocusEnabled(false);
        jp_progress.setVerifyInputWhenFocusTarget(false);

        javax.swing.GroupLayout jp_progressLayout = new javax.swing.GroupLayout(jp_progress);
        jp_progress.setLayout(jp_progressLayout);
        jp_progressLayout.setHorizontalGroup(
            jp_progressLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );
        jp_progressLayout.setVerticalGroup(
            jp_progressLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );

        corps.add(jp_progress, new org.netbeans.lib.awtextra.AbsoluteConstraints(1110, 190, 160, 160));

        jPanel3.setBackground(new java.awt.Color(237, 237, 237));
        jPanel3.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(153, 0, 0), 2, true));
        jPanel3.setForeground(new java.awt.Color(153, 0, 0));
        jPanel3.setFocusCycleRoot(true);
        jPanel3.setFocusTraversalPolicyProvider(true);
        jPanel3.setInheritsPopupMenu(true);
        jPanel3.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        question.setEditable(false);
        question.setBackground(new java.awt.Color(240, 240, 240));
        question.setColumns(20);
        question.setFont(new java.awt.Font("Segoe UI Symbol", 1, 23)); // NOI18N
        question.setLineWrap(true);
        question.setRows(3);
        question.setText("\n\t\tBienvenue à la semaine Olympique Guinéenne\n\t\t\t     2018/2019");
        question.setToolTipText("");
        question.setBorder(null);
        question.setCaretColor(new java.awt.Color(240, 240, 240));
        question.setDisabledTextColor(null);
        question.setEnabled(false);
        question.setFocusable(false);
        question.setMargin(new java.awt.Insets(15, 15, 15, 15));
        question.setMinimumSize(new java.awt.Dimension(130, 61));
        question.setName(""); // NOI18N
        question.setOpaque(false);
        question.setRequestFocusEnabled(false);
        question.setSelectionColor(null);
        question.setVerifyInputWhenFocusTarget(false);
        jScrollPane2.setViewportView(question);

        jPanel3.add(jScrollPane2, new org.netbeans.lib.awtextra.AbsoluteConstraints(23, 49, 1160, 109));

        reponse_panel.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        reponse_panel.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        reponse0.setFont(new java.awt.Font("Segoe UI Symbol", 1, 23)); // NOI18N
        reponse0.setText("reponse0");
        reponse0.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        reponse0.setRequestFocusEnabled(false);
        reponse_panel.add(reponse0, new org.netbeans.lib.awtextra.AbsoluteConstraints(1, 16, 1157, 38));

        reponse2.setFont(new java.awt.Font("Segoe UI Symbol", 1, 23)); // NOI18N
        reponse2.setText("reponse0");
        reponse2.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        reponse2.setRequestFocusEnabled(false);
        reponse_panel.add(reponse2, new org.netbeans.lib.awtextra.AbsoluteConstraints(1, 114, 1157, 38));

        reponse1.setFont(new java.awt.Font("Segoe UI Symbol", 1, 23)); // NOI18N
        reponse1.setText("reponse0");
        reponse1.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        reponse1.setRequestFocusEnabled(false);
        reponse_panel.add(reponse1, new org.netbeans.lib.awtextra.AbsoluteConstraints(1, 65, 1157, 38));

        reponse3.setFont(new java.awt.Font("Segoe UI Symbol", 1, 23)); // NOI18N
        reponse3.setText("reponse0");
        reponse3.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        reponse_panel.add(reponse3, new org.netbeans.lib.awtextra.AbsoluteConstraints(1, 160, 1157, 38));
        reponse3.getAccessibleContext().setAccessibleDescription("");

        jPanel3.add(reponse_panel, new org.netbeans.lib.awtextra.AbsoluteConstraints(23, 165, 1160, 210));

        jLabel13.setFont(new java.awt.Font("Agency FB", 1, 26)); // NOI18N
        jLabel13.setForeground(new java.awt.Color(153, 0, 0));
        jLabel13.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jPanel3.add(jLabel13, new org.netbeans.lib.awtextra.AbsoluteConstraints(467, 2, 349, 38));

        corps.add(jPanel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 300, 1200, 390));

        jPanel5.setBackground(new java.awt.Color(204, 255, 204));
        jPanel5.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel6.setFont(new java.awt.Font("Agency FB", 1, 28)); // NOI18N
        jLabel6.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/Lungs_33px.png"))); // NOI18N
        jLabel6.setText("Biologie");
        jLabel6.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jLabel6.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jLabel6.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel6MouseClicked(evt);
            }
        });
        jPanel5.add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(200, 0, 180, 50));

        jLabel21.setFont(new java.awt.Font("Agency FB", 1, 28)); // NOI18N
        jLabel21.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel21.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/Math_33px.png"))); // NOI18N
        jLabel21.setText("Mathématiques");
        jLabel21.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jLabel21.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jLabel21.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel21MouseClicked(evt);
            }
        });
        jPanel5.add(jLabel21, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 201, 50));

        jLabel7.setFont(new java.awt.Font("Agency FB", 1, 28)); // NOI18N
        jLabel7.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel7.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/Geography_33px.png"))); // NOI18N
        jLabel7.setText("Sciences sociales");
        jLabel7.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jLabel7.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jLabel7.setRequestFocusEnabled(false);
        jLabel7.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel7MouseClicked(evt);
            }
        });
        jPanel5.add(jLabel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(380, 0, 221, 50));

        jLabel23.setFont(new java.awt.Font("Agency FB", 1, 28)); // NOI18N
        jLabel23.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel23.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/Laptop_33px.png"))); // NOI18N
        jLabel23.setText("NTICs");
        jLabel23.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jLabel23.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jLabel23.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel23MouseClicked(evt);
            }
        });
        jPanel5.add(jLabel23, new org.netbeans.lib.awtextra.AbsoluteConstraints(600, 0, 130, 50));

        corps.add(jPanel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(370, 240, 730, 50));

        labelEquipeA.setFont(new java.awt.Font("Agency FB", 1, 36)); // NOI18N
        labelEquipeA.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        labelEquipeA.setText("Equipe A");
        labelEquipeA.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        corps.add(labelEquipeA, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 700, 350, 47));

        retrait_joueur1.setFont(new java.awt.Font("Agency FB", 1, 28)); // NOI18N
        retrait_joueur1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        retrait_joueur1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Minus_33px_1.png"))); // NOI18N
        retrait_joueur1.setBorder(javax.swing.BorderFactory.createBevelBorder(0, java.awt.Color.gray, java.awt.Color.gray, java.awt.Color.gray, java.awt.Color.gray));
        retrait_joueur1.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        retrait_joueur1.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        retrait_joueur1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                retrait_joueur1MouseClicked(evt);
            }
        });
        corps.add(retrait_joueur1, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 860, 50, 40));

        ajout_joueur1.setFont(new java.awt.Font("Agency FB", 1, 28)); // NOI18N
        ajout_joueur1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        ajout_joueur1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/Plus_33px_1.png"))); // NOI18N
        ajout_joueur1.setBorder(javax.swing.BorderFactory.createBevelBorder(0, java.awt.Color.gray, java.awt.Color.gray, java.awt.Color.gray, java.awt.Color.gray));
        ajout_joueur1.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        ajout_joueur1.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        ajout_joueur1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                ajout_joueur1MouseClicked(evt);
            }
        });
        corps.add(ajout_joueur1, new org.netbeans.lib.awtextra.AbsoluteConstraints(210, 860, 50, 40));

        ajout_joueur2.setFont(new java.awt.Font("Agency FB", 1, 28)); // NOI18N
        ajout_joueur2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        ajout_joueur2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/Plus_33px_1.png"))); // NOI18N
        ajout_joueur2.setBorder(javax.swing.BorderFactory.createBevelBorder(0, java.awt.Color.gray, java.awt.Color.gray, java.awt.Color.gray, java.awt.Color.gray));
        ajout_joueur2.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        ajout_joueur2.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        ajout_joueur2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                ajout_joueur2MouseClicked(evt);
            }
        });
        corps.add(ajout_joueur2, new org.netbeans.lib.awtextra.AbsoluteConstraints(620, 860, 50, 40));

        retrait_joueur2.setFont(new java.awt.Font("Agency FB", 1, 28)); // NOI18N
        retrait_joueur2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        retrait_joueur2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Minus_33px_1.png"))); // NOI18N
        retrait_joueur2.setBorder(javax.swing.BorderFactory.createBevelBorder(0, java.awt.Color.gray, java.awt.Color.gray, java.awt.Color.gray, java.awt.Color.gray));
        retrait_joueur2.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        retrait_joueur2.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        retrait_joueur2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                retrait_joueur2MouseClicked(evt);
            }
        });
        corps.add(retrait_joueur2, new org.netbeans.lib.awtextra.AbsoluteConstraints(510, 860, 50, 40));

        label_erreur.setFont(new java.awt.Font("Agency FB", 1, 26)); // NOI18N
        label_erreur.setForeground(new java.awt.Color(153, 0, 0));
        label_erreur.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        corps.add(label_erreur, new org.netbeans.lib.awtextra.AbsoluteConstraints(489, 720, 330, 38));

        retrait_joueur3.setFont(new java.awt.Font("Agency FB", 1, 28)); // NOI18N
        retrait_joueur3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        retrait_joueur3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Minus_33px_1.png"))); // NOI18N
        retrait_joueur3.setBorder(javax.swing.BorderFactory.createBevelBorder(0, java.awt.Color.gray, java.awt.Color.gray, java.awt.Color.gray, java.awt.Color.gray));
        retrait_joueur3.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        retrait_joueur3.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        retrait_joueur3.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                retrait_joueur3MouseClicked(evt);
            }
        });
        corps.add(retrait_joueur3, new org.netbeans.lib.awtextra.AbsoluteConstraints(910, 860, 50, 40));

        ajout_joueur3.setFont(new java.awt.Font("Agency FB", 1, 28)); // NOI18N
        ajout_joueur3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        ajout_joueur3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/Plus_33px_1.png"))); // NOI18N
        ajout_joueur3.setBorder(javax.swing.BorderFactory.createBevelBorder(0, java.awt.Color.gray, java.awt.Color.gray, java.awt.Color.gray, java.awt.Color.gray));
        ajout_joueur3.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        ajout_joueur3.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        ajout_joueur3.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                ajout_joueur3MouseClicked(evt);
            }
        });
        corps.add(ajout_joueur3, new org.netbeans.lib.awtextra.AbsoluteConstraints(1020, 860, 50, 40));

        scoreC.setFont(new java.awt.Font("Agency FB", 1, 120)); // NOI18N
        scoreC.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        scoreC.setText("0");
        scoreC.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        corps.add(scoreC, new org.netbeans.lib.awtextra.AbsoluteConstraints(880, 750, 220, 100));

        labelEquipeC.setFont(new java.awt.Font("Agency FB", 1, 36)); // NOI18N
        labelEquipeC.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        labelEquipeC.setText("Equipe C");
        labelEquipeC.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        corps.add(labelEquipeC, new org.netbeans.lib.awtextra.AbsoluteConstraints(820, 700, 340, 47));

        parametre.add(corps, new org.netbeans.lib.awtextra.AbsoluteConstraints(310, 0, 1280, -1));

        menu.setBackground(new java.awt.Color(153, 0, 0));
        menu.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel4.setBackground(new java.awt.Color(237, 237, 237));

        jLabel4.setFont(new java.awt.Font("Agency FB", 1, 22)); // NOI18N
        jLabel4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/Hourglass_28px.png"))); // NOI18N
        jLabel4.setText("Minuterie");

        jLabel14.setFont(new java.awt.Font("Agency FB", 1, 22)); // NOI18N
        jLabel14.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/User Groups_28px.png"))); // NOI18N
        jLabel14.setText("Les équipes");

        labeEquipeB_in.setFont(new java.awt.Font("Agency FB", 1, 20)); // NOI18N
        labeEquipeB_in.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        labeEquipeB_in.setText("nom de l'équipe B");
        labeEquipeB_in.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                labeEquipeB_inActionPerformed(evt);
            }
        });

        labeEquipeA_in.setFont(new java.awt.Font("Agency FB", 1, 20)); // NOI18N
        labeEquipeA_in.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        labeEquipeA_in.setText("nom de l'équipe A");
        labeEquipeA_in.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                labeEquipeA_inActionPerformed(evt);
            }
        });

        labelChrono.setFont(new java.awt.Font("Agency FB", 1, 20)); // NOI18N
        labelChrono.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        labelChrono.setText("durée (en s)");
        labelChrono.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                labelChronoActionPerformed(evt);
            }
        });

        jLabel27.setBackground(new java.awt.Color(153, 0, 0));
        jLabel27.setFont(new java.awt.Font("Agency FB", 1, 28)); // NOI18N
        jLabel27.setForeground(new java.awt.Color(255, 255, 255));
        jLabel27.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel27.setText("Valider");
        jLabel27.setAutoscrolls(true);
        jLabel27.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 0, 0), 1, true));
        jLabel27.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jLabel27.setDoubleBuffered(true);
        jLabel27.setFocusCycleRoot(true);
        jLabel27.setFocusTraversalPolicyProvider(true);
        jLabel27.setOpaque(true);
        jLabel27.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel27MouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                jLabel27MouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                jLabel27MouseExited(evt);
            }
        });

        labelErreur.setEditable(false);
        labelErreur.setFont(new java.awt.Font("Agency FB", 1, 20)); // NOI18N
        labelErreur.setForeground(new java.awt.Color(255, 0, 0));
        labelErreur.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        labelErreur.setBorder(null);
        labelErreur.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                labelErreurActionPerformed(evt);
            }
        });

        jSeparator1.setForeground(new java.awt.Color(0, 0, 0));

        jSeparator4.setForeground(new java.awt.Color(0, 0, 0));

        labeEquipeC_in.setFont(new java.awt.Font("Agency FB", 1, 20)); // NOI18N
        labeEquipeC_in.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        labeEquipeC_in.setText("nom de l'équipe C");
        labeEquipeC_in.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                labeEquipeC_inActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGap(85, 85, 85)
                        .addComponent(jLabel14, javax.swing.GroupLayout.PREFERRED_SIZE, 116, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 97, Short.MAX_VALUE))
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jSeparator4, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(labelErreur, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jSeparator1, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(labeEquipeB_in, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(labeEquipeA_in, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(labeEquipeC_in))))
                .addContainerGap())
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                        .addComponent(jLabel4)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(labelChrono, javax.swing.GroupLayout.PREFERRED_SIZE, 99, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(28, 28, 28))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                        .addComponent(jLabel27, javax.swing.GroupLayout.PREFERRED_SIZE, 92, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(108, 108, 108))))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGap(19, 19, 19)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel14)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(labeEquipeA_in, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(12, 12, 12)
                .addComponent(labeEquipeB_in, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(labeEquipeC_in, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(36, 36, 36)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(labelChrono, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(jLabel27)
                .addGap(61, 61, 61)
                .addComponent(labelErreur, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jSeparator4, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        menu.add(jPanel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 160, 310, 390));

        jLabel10.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/Menu_32px_2.png"))); // NOI18N
        jLabel10.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jLabel10.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jLabel10.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel10MouseClicked(evt);
            }
        });
        menu.add(jLabel10, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 110, -1, -1));
        menu.add(jSeparator2, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 150, 270, 20));

        jLabel16.setFont(new java.awt.Font("Agency FB", 1, 22)); // NOI18N
        jLabel16.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/Info_33px_1.png"))); // NOI18N
        jLabel16.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jLabel16.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jLabel16.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel16MouseClicked(evt);
            }
        });
        menu.add(jLabel16, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 110, -1, -1));

        jLabel17.setFont(new java.awt.Font("Agency FB", 1, 22)); // NOI18N
        jLabel17.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/Logout Rounded Left_33px.png"))); // NOI18N
        jLabel17.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jLabel17.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jLabel17.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel17MouseClicked(evt);
            }
        });
        menu.add(jLabel17, new org.netbeans.lib.awtextra.AbsoluteConstraints(250, 110, 40, -1));
        menu.add(jSeparator3, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 100, 270, 20));

        jLabel28.setFont(new java.awt.Font("Agency FB", 1, 22)); // NOI18N
        jLabel28.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/Settings_33px_1.png"))); // NOI18N
        jLabel28.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jLabel28.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jLabel28.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel28MouseClicked(evt);
            }
        });
        menu.add(jLabel28, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 110, -1, -1));

        jLabel22.setFont(new java.awt.Font("Agency FB", 1, 20)); // NOI18N
        jLabel22.setForeground(new java.awt.Color(255, 255, 255));
        jLabel22.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel22.setText("<< La force de l'excellence >>");
        jLabel22.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        menu.add(jLabel22, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 50, 210, 40));

        jLabel20.setBackground(new java.awt.Color(255, 255, 255));
        jLabel20.setFont(new java.awt.Font("Brush Script MT", 1, 30)); // NOI18N
        jLabel20.setForeground(new java.awt.Color(255, 255, 255));
        jLabel20.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel20.setText("Olympiades 2019  ");
        menu.add(jLabel20, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 20, 230, -1));

        jLabel15.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/Student Male_70px_1.png"))); // NOI18N
        menu.add(jLabel15, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 0, -1, 80));

        bt_run.setBackground(new java.awt.Color(255, 255, 255));
        bt_run.setFont(new java.awt.Font("Agency FB", 1, 28)); // NOI18N
        bt_run.setForeground(new java.awt.Color(153, 0, 0));
        bt_run.setText("Démarrer");
        bt_run.setAutoscrolls(true);
        bt_run.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt_run.setDefaultCapable(false);
        bt_run.setDoubleBuffered(true);
        bt_run.setFocusPainted(false);
        bt_run.setRequestFocusEnabled(false);
        bt_run.setRolloverEnabled(false);
        bt_run.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                bt_runMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                bt_runMouseExited(evt);
            }
        });
        bt_run.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt_runActionPerformed(evt);
            }
        });
        menu.add(bt_run, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 720, 215, 70));
        bt_run.getAccessibleContext().setAccessibleDescription("");

        jButton2.setBackground(new java.awt.Color(255, 255, 255));
        jButton2.setFont(new java.awt.Font("Agency FB", 1, 28)); // NOI18N
        jButton2.setForeground(new java.awt.Color(153, 0, 0));
        jButton2.setText("Arrêter");
        jButton2.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jButton2.setFocusable(false);
        jButton2.setRequestFocusEnabled(false);
        jButton2.setRolloverEnabled(false);
        jButton2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jButton2MouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                jButton2MouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                jButton2MouseExited(evt);
            }
        });
        menu.add(jButton2, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 720, 215, 70));
        jButton2.getAccessibleContext().setAccessibleDescription("");

        jButton3.setBackground(new java.awt.Color(255, 255, 255));
        jButton3.setFont(new java.awt.Font("Agency FB", 1, 28)); // NOI18N
        jButton3.setForeground(new java.awt.Color(153, 0, 0));
        jButton3.setText("Bonne(s) réponses");
        jButton3.setBorderPainted(false);
        jButton3.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jButton3.setFocusable(false);
        jButton3.setRequestFocusEnabled(false);
        jButton3.setRolloverEnabled(false);
        jButton3.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jButton3MouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                jButton3MouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                jButton3MouseExited(evt);
            }
        });
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });
        menu.add(jButton3, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 810, 215, 70));
        jButton3.getAccessibleContext().setAccessibleDescription("");

        parametre.add(menu, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 310, 950));

        principal_bleu.add(parametre, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 1590, 950));
        parametre.getAccessibleContext().setAccessibleName("");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(principal_bleu, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(principal_bleu, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

  
    //**************************************************************************************************************************************************************
    //******************************************************************************PAS TOUCHE EN HAUT***************************************************************
    //**************************************************************************************************************************************************************
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    public static int nombreAleatoire=1; 
    ArrayList<Integer> tabRandom =new ArrayList<Integer>();   
    
    public static boolean isClicked=false;
    public static int num_=60;
    public static int rubrique=0; // par défaut la rubrique maths est choisie
    public static String bonneReponse0,bonneReponse1,bonneReponse2,bonneReponse3 ;
    
    
    //--------------------------------------------------bouton Démarrer
    Timer t;
    static int count=num_;//par défaut ça commence à 60
    private void bt_runActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt_runActionPerformed
        bt_run.setVisible(false);
        question.setVisible(true);
        reponse_panel.setVisible(true);          
        jButton2.setVisible(true);
        //remettre les couleurs par défauts 
        reponse0.setBackground(new Color(237,237,237));
        reponse0.setOpaque(true);
        reponse0.setBorder(BorderFactory.createBevelBorder(1,new Color(237,237,237), new Color(237,237,237), new Color(237,237,237), new Color(237,237,237)));
        reponse0.setForeground(new Color(0,0,0));      

        reponse1.setBackground(new Color(237,237,237));
        reponse1.setOpaque(true);
        reponse1.setBorder(BorderFactory.createBevelBorder(1,new Color(237,237,237), new Color(237,237,237), new Color(237,237,237), new Color(237,237,237)));
        reponse1.setForeground(new Color(0,0,0));

        reponse2.setBackground(new Color(237,237,237));
        reponse2.setOpaque(true);
        reponse2.setBorder(BorderFactory.createBevelBorder(1,new Color(237,237,237), new Color(237,237,237), new Color(237,237,237), new Color(237,237,237)));
        reponse2.setForeground(new Color(0,0,0));

        reponse3.setBackground(new Color(237,237,237));
        reponse3.setOpaque(true);
        reponse3.setBorder(BorderFactory.createBevelBorder(1,new Color(237,237,237), new Color(237,237,237), new Color(237,237,237), new Color(237,237,237)));
        reponse3.setForeground(new Color(0,0,0));

        reponse0.setText("");
        reponse2.setText("");
        reponse1.setText("");
        reponse3.setText("");
        
        //si on pas choisi de rubrique, afficher une erreur
        if(rubrique!=0 && rubrique!=1 && rubrique!=2 && rubrique!=3)
        {
            label_erreur.setText("Choisir une rubrique");
        }
        else
        {
            //_____________________________________________________________________________________________________________________________________________________________________
            //si rubrique=0; on recherche uniquement dans la table maths
            //_____________________________________________________________________________________________________________________________________________________________________
            if(rubrique==0)
            {
                //_______________________________________________debut du chrono
                t=new Timer(1000,new ActionListener() 
                {
                    @Override
                    public void actionPerformed(ActionEvent e) 
                    {                         
                        count--;
                        jp_progress.UpdateProgress(count);
                        jp_progress.repaint();
                        //si le temps s'est écoulé
                        if(count==0)
                        {   
                            //arreter le thread
                            t.stop();
                            //remettre le compteur à la valeur de num_
                            count=num_;
                            
                            //----------------cacher les question quand le temps fini de s'écouler
                            question.setVisible(false);
                        }                      
                    }
                });
                t.start();
                //_________________________________________________fin du chrono
                
                
                
              
                        
                         //---------------------------------------variable de controle
                         //variable permettant d'indiquer si on a charger les questions oui ou non, dans le cas de oui alors on devra afficher les resultats quand on appuie sur arreter
                         //------------------------------------------------------------------------affichage des questions
                        isClicked=true;//on met à true pour dire qu'on a cliqué sur le bouton Démarrer 
                        Connection connection;
                        Statement statement;
                        ResultSet result;
                        ResultSet result1;

                        try {
                                Class.forName("com.mysql.cj.jdbc.Driver").newInstance();          
                                connection=DriverManager.getConnection("jdbc:mysql://localhost/olympiades?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC", "root", "");
                                statement = connection.createStatement();


                                //trouver le nombre d'enregistrement de la table et le stocker dans la variable Max
                                int Min=1, Max=0;
                                //String reqt_nbrEnregis= "SELECT COUNT(DISTINCT nom_colonne) FROM table";        
                                result1 = statement.executeQuery("SELECT COUNT(DISTINCT contenu_question_maths) AS compte FROM question_maths;");

                                //tant que y'a un contenu, le ramener
                               while(result1.next())
                               {
                                    Max=result1.getInt("compte");     
                               }

                               //on initialise le tableau à 0 car on est sur qu'aucun nombre généreé ne sera null
                               tabRandom.add(0);

                               //générer un nombre aléatoire 'aleatoire' entre Min et Max et accéder à l'enrengistrement numéro
                               nombreAleatoire = Min + (int)(Math.random() * ((Max - Min) + 1));
                               tabRandom.add(nombreAleatoire);

                               String requete="SELECT distinct contenu_question_maths from question_maths where id_question_maths=" +nombreAleatoire;
                               result = statement.executeQuery(requete); 

                                //tant que y'a un contenu, le ramener
                               while(result.next()){
                                question.setText(" "+result.getString("contenu_question_maths"));      
                                }  
                        } catch (Exception e) {
                           e.printStackTrace();
                        }
                        
                        
                        
                         //________________________________________________affichage des questions en même temps que les réponses

                        {

                            ArrayList<String> lesReponses =new ArrayList<String>(); //tabpaleau dynamique de chaine de caractère qui doit contenir une reponse par indice
                            ArrayList<String> Reponse_valide =new ArrayList<String>();
                            try {
                                    Class.forName("com.mysql.cj.jdbc.Driver").newInstance();          
                                    connection=DriverManager.getConnection("jdbc:mysql://localhost/olympiades?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC", "root", "");
                                    statement = connection.createStatement();

                                         //-----------------on suppose 4 réponses dans l'application, on nettoie le tableau à chaque nouvel apppui
                                        lesReponses.add(0,"0");
                                        lesReponses.add(1,"0");
                                        lesReponses.add(2,"0");
                                        lesReponses.add(3,"0");

                                        Reponse_valide.add(0,"0");
                                        Reponse_valide.add(1,"0");
                                        Reponse_valide.add(2,"0");
                                        Reponse_valide.add(3,"0");    

                                    //table reponse
                                    String requete="SELECT * from reponse_maths where  id_question_maths=" +nombreAleatoire;
                                    result = statement.executeQuery(requete); 

                                     //tant que y'a un contenu à lire de la BD, on le lit et on stocke dans le tableau dynamique
                                    int j = 0;
                                   // lesReponses.clear();
                                    //Reponse_valide.clear();
                                    while(result.next())
                                    {     
                                        lesReponses.set(j, result.getString("contenu_reponse_maths"));
                                        Reponse_valide.set(j,result.getString("bingo_reponse_maths"));
                                        j++;
                                    }  
                                    //affichage des reponses sur l'interface graphique
                                    reponse0.setText(" "+lesReponses.get(0)+" ");
                                    reponse1.setText(" "+lesReponses.get(1)+" ");
                                    reponse2.setText(" "+lesReponses.get(2)+" ");
                                    reponse3.setText(" "+lesReponses.get(3)+" ");

                                   //recuperer les valeurs pour pouvoir mettre les bonne reponsnes en surbriallance quand le chorno finira
                                    bonneReponse0=Reponse_valide.get(0);
                                    bonneReponse1=Reponse_valide.get(1);
                                    bonneReponse2=Reponse_valide.get(2);
                                    bonneReponse3=Reponse_valide.get(3);

                                    lesReponses.clear();
                                    Reponse_valide.clear();

                                    connection.close();
                                    //---------------------------------------------------------------------------------------------------------fin marquage bonne reponse

                                 }
                                catch(Exception e)
                                {
                                    e.printStackTrace();
                                }
                        }//fin affichage des questions                                             
            }//fin if==0 pour les maths___________________________________________________________________________________________________________________________________________
             //___________________________________________________________________________________________________________________________________________________________________
        
        
        
             
             
             
             
             
             
             
             
             
             
             
             
             
             
             //_____________________________________________________________________________________________________________________________________________________________________
            //si rubrique=0; on recherche uniquement dans la table biologie
            //_____________________________________________________________________________________________________________________________________________________________________
            if(rubrique==1)
            {
                //_______________________________________________debut du chrono
                t=new Timer(1000,new ActionListener() 
                {
                    @Override
                    public void actionPerformed(ActionEvent e) 
                    {                         
                        count--;
                        jp_progress.UpdateProgress(count);
                        jp_progress.repaint();
                        //si le temps s'est écoulé
                        if(count==0)
                        {   
                            //arreter le thread
                            t.stop();
                            //remettre le compteur à la valeur de num_
                            count=num_;
                            
                            //----------------cacher les question quand le temps fini de s'écouler
                            question.setVisible(false);
                        }                      
                    }
                });
                t.start();
                //_________________________________________________fin du chrono
                
                
                
              
                        
                         //---------------------------------------variable de controle
                         //variable permettant d'indiquer si on a charger les questions oui ou non, dans le cas de oui alors on devra afficher les resultats quand on appuie sur arreter
                         //------------------------------------------------------------------------affichage des questions
                        isClicked=true;//on met à true pour dire qu'on a cliqué sur le bouton Démarrer 
                        Connection connection;
                        Statement statement;
                        ResultSet result;
                        ResultSet result1;

                        try {
                                Class.forName("com.mysql.cj.jdbc.Driver").newInstance();          
                                connection=DriverManager.getConnection("jdbc:mysql://localhost/olympiades?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC", "root", "");
                                statement = connection.createStatement();


                                //trouver le nombre d'enregistrement de la table et le stocker dans la variable Max
                                int Min=1, Max=0;
                                //String reqt_nbrEnregis= "SELECT COUNT(DISTINCT nom_colonne) FROM table";        
                                result1 = statement.executeQuery("SELECT COUNT(DISTINCT contenu_question_biologie) AS compte FROM question_biologie;");

                                //tant que y'a un contenu, le ramener
                               while(result1.next())
                               {
                                    Max=result1.getInt("compte");     
                               }

                               //on initialise le tableau à 0 car on est sur qu'aucun nombre généreé ne sera null
                               tabRandom.add(0);

                               //générer un nombre aléatoire 'aleatoire' entre Min et Max et accéder à l'enrengistrement numéro
                               nombreAleatoire = Min + (int)(Math.random() * ((Max - Min) + 1));
                               tabRandom.add(nombreAleatoire);

                               String requete="SELECT distinct contenu_question_biologie from question_biologie where id_question_biologie=" +nombreAleatoire;
                               result = statement.executeQuery(requete); 

                                //tant que y'a un contenu, le ramener
                               while(result.next()){
                                question.setText(" "+result.getString("contenu_question_biologie"));      
                                }  
                        } catch (Exception e) {
                           e.printStackTrace();
                        }
                        
                        
                        
                         //________________________________________________affichage des questions en même temps que les réponses

                        {

                            ArrayList<String> lesReponses =new ArrayList<String>(); //tabpaleau dynamique de chaine de caractère qui doit contenir une reponse par indice
                            ArrayList<String> Reponse_valide =new ArrayList<String>();
                            try {
                                    Class.forName("com.mysql.cj.jdbc.Driver").newInstance();          
                                    connection=DriverManager.getConnection("jdbc:mysql://localhost/olympiades?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC", "root", "");
                                    statement = connection.createStatement();

                                         //-----------------on suppose 4 réponses dans l'application, on nettoie le tableau à chaque nouvel apppui
                                        lesReponses.add(0,"0");
                                        lesReponses.add(1,"0");
                                        lesReponses.add(2,"0");
                                        lesReponses.add(3,"0");

                                        Reponse_valide.add(0,"0");
                                        Reponse_valide.add(1,"0");
                                        Reponse_valide.add(2,"0");
                                        Reponse_valide.add(3,"0");    

                                    //table reponse
                                    String requete="SELECT * from reponse_biologie where  id_question_biologie=" +nombreAleatoire;
                                    result = statement.executeQuery(requete); 

                                     //tant que y'a un contenu à lire de la BD, on le lit et on stocke dans le tableau dynamique
                                    int j = 0;
                                   // lesReponses.clear();
                                    //Reponse_valide.clear();
                                    while(result.next())
                                    {     
                                        lesReponses.set(j, result.getString("contenu_reponse_biologie"));
                                        Reponse_valide.set(j,result.getString("bingo_reponse_biologie"));
                                        j++;
                                    }  
                                    //affichage des reponses sur l'interface graphique
                                    reponse0.setText(" "+lesReponses.get(0)+" ");
                                    reponse1.setText(" "+lesReponses.get(1)+" ");
                                    reponse2.setText(" "+lesReponses.get(2)+" ");
                                    reponse3.setText(" "+lesReponses.get(3)+" ");

                                   //recuperer les valeurs pour pouvir mettre les bonne reponsnes en surbriallance quand le chorno finira
                                    bonneReponse0=Reponse_valide.get(0);
                                    bonneReponse1=Reponse_valide.get(1);
                                    bonneReponse2=Reponse_valide.get(2);
                                    bonneReponse3=Reponse_valide.get(3);

                                    lesReponses.clear();
                                    Reponse_valide.clear();

                                    connection.close();
                                    //---------------------------------------------------------------------------------------------------------fin marquage bonne reponse

                                 }
                                catch(Exception e)
                                {
                                    e.printStackTrace();
                                }
                        }//fin affichage des questions                                             
            }//fin if==1 pour les biologie
             //___________________________________________________________________________________________________________________________________________________________________
        
             
             
             
             
             
             
             
             
             
             
             
             
             
             
             
             
             //_____________________________________________________________________________________________________________________________________________________________________
            //si rubrique=0; on recherche uniquement dans la table sciences sociales
            //_____________________________________________________________________________________________________________________________________________________________________
            if(rubrique==2)
            {
                //_______________________________________________debut du chrono
                t=new Timer(1000,new ActionListener() 
                {
                    @Override
                    public void actionPerformed(ActionEvent e) 
                    {                         
                        count--;
                        jp_progress.UpdateProgress(count);
                        jp_progress.repaint();
                        //si le temps s'est écoulé
                        if(count==0)
                        {   
                            //arreter le thread
                            t.stop();
                            //remettre le compteur à la valeur de num_
                            count=num_;
                            
                            //----------------cacher les question quand le temps fini de s'écouler
                            question.setVisible(false);
                        }                      
                    }
                });
                t.start();
                //_________________________________________________fin du chrono
                
                
                
              
                        
                         //---------------------------------------variable de controle
                         //variable permettant d'indiquer si on a charger les questions oui ou non, dans le cas de oui alors on devra afficher les resultats quand on appuie sur arreter
                         //------------------------------------------------------------------------affichage des questions
                        isClicked=true;//on met à true pour dire qu'on a cliqué sur le bouton Démarrer 
                        Connection connection;
                        Statement statement;
                        ResultSet result;
                        ResultSet result1;

                        try {
                                Class.forName("com.mysql.cj.jdbc.Driver").newInstance();          
                                connection=DriverManager.getConnection("jdbc:mysql://localhost/olympiades?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC", "root", "");
                                statement = connection.createStatement();


                                //trouver le nombre d'enregistrement de la table et le stocker dans la variable Max
                                int Min=1, Max=0;
                                //String reqt_nbrEnregis= "SELECT COUNT(DISTINCT nom_colonne) FROM table";        
                                result1 = statement.executeQuery("SELECT COUNT(DISTINCT contenu_question_science_sociale) AS compte FROM question_science_sociale;");

                                //tant que y'a un contenu, le ramener
                               while(result1.next())
                               {
                                    Max=result1.getInt("compte");     
                               }

                               //on initialise le tableau à 0 car on est sur qu'aucun nombre généreé ne sera null
                               tabRandom.add(0);

                               //générer un nombre aléatoire 'aleatoire' entre Min et Max et accéder à l'enrengistrement numéro
                               nombreAleatoire = Min + (int)(Math.random() * ((Max - Min) + 1));
                               tabRandom.add(nombreAleatoire);

                                /*int sortir=0;
                               //tant que la question a été posée, on cherche une autre question non encore posée
                               while(tabRandom.contains(nombreAleatoire) && sortir==0)
                               {
                                   //si le nombre existe, on genere un nombre puis on verifie à nouveau si il est déjà dans le tableau
                                    nombreAleatoire = Min + (int)(Math.random() * ((Max - Min) + 1));
                                    if(!(tabRandom.contains(nombreAleatoire)))
                                    {
                                        tabRandom.add(nombreAleatoire);
                                        sortir=1;
                                    }
                               }//on sort avec le bon nombre et on l'insère dans le tableau
                               */

                               String requete="SELECT distinct contenu_question_science_sociale from question_science_sociale where id_question_science_sociale=" +nombreAleatoire;
                               result = statement.executeQuery(requete); 

                                //tant que y'a un contenu, le ramener
                               while(result.next()){
                                question.setText(" "+result.getString("contenu_question_science_sociale"));      
                                }  
                        } catch (Exception e) {
                           e.printStackTrace();
                        }
                        
                        
                        
                         //________________________________________________affichage des questions en même temps que les réponses

                        {

                            ArrayList<String> lesReponses =new ArrayList<String>(); //tabpaleau dynamique de chaine de caractère qui doit contenir une reponse par indice
                            ArrayList<String> Reponse_valide =new ArrayList<String>();
                            try {
                                    Class.forName("com.mysql.cj.jdbc.Driver").newInstance();          
                                    connection=DriverManager.getConnection("jdbc:mysql://localhost/olympiades?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC", "root", "");
                                    statement = connection.createStatement();

                                         //-----------------on suppose 4 réponses dans l'application, on nettoie le tableau à chaque nouvel apppui
                                        lesReponses.add(0,"0");
                                        lesReponses.add(1,"0");
                                        lesReponses.add(2,"0");
                                        lesReponses.add(3,"0");

                                        Reponse_valide.add(0,"0");
                                        Reponse_valide.add(1,"0");
                                        Reponse_valide.add(2,"0");
                                        Reponse_valide.add(3,"0");    

                                    //table reponse
                                    String requete="SELECT * from reponse_science_sociale where  id_question_science_sociale=" +nombreAleatoire;
                                    result = statement.executeQuery(requete); 

                                     //tant que y'a un contenu à lire de la BD, on le lit et on stocke dans le tableau dynamique
                                    int j = 0;
                                   // lesReponses.clear();
                                    //Reponse_valide.clear();
                                    while(result.next())
                                    {     
                                        lesReponses.set(j, result.getString("contenu_reponse_science_sociale"));
                                        Reponse_valide.set(j,result.getString("bingo_reponse_science_sociale"));
                                        j++;
                                    }  
                                    //affichage des reponses sur l'interface graphique
                                    reponse0.setText(" "+lesReponses.get(0)+" ");
                                    reponse1.setText(" "+lesReponses.get(1)+" ");
                                    reponse2.setText(" "+lesReponses.get(2)+" ");
                                    reponse3.setText(" "+lesReponses.get(3)+" ");

                                   //recuperer les valeurs pour pouvir mettre les bonne reponsnes en surbriallance quand le chorno finira
                                    bonneReponse0=Reponse_valide.get(0);
                                    bonneReponse1=Reponse_valide.get(1);
                                    bonneReponse2=Reponse_valide.get(2);
                                    bonneReponse3=Reponse_valide.get(3);

                                    lesReponses.clear();
                                    Reponse_valide.clear();

                                    connection.close();
                                    //---------------------------------------------------------------------------------------------------------fin marquage bonne reponse

                                 }
                                catch(Exception e)
                                {
                                    e.printStackTrace();
                                }
                        }//fin affichage des questions                                             
            }//fin if==2 pour les sciences_sociales
             //___________________________________________________________________________________________________________________________________________________________________
        
             
             
             
             
             
             
             
             
             
             
             
             
             
             
             //_____________________________________________________________________________________________________________________________________________________________________
            //si rubrique=0; on recherche uniquement dans la table ntic
            //_____________________________________________________________________________________________________________________________________________________________________
            if(rubrique==3)
            {
                //_______________________________________________debut du chrono
                t=new Timer(1000,new ActionListener() 
                {
                    @Override
                    public void actionPerformed(ActionEvent e) 
                    {                         
                        count--;
                        jp_progress.UpdateProgress(count);
                        jp_progress.repaint();
                        //si le temps s'est écoulé
                        if(count==0)
                        {   
                            //arreter le thread
                            t.stop();
                            //remettre le compteur à la valeur de num_
                            count=num_;
                            
                            //----------------cacher les question quand le temps fini de s'écouler
                            question.setVisible(false);
                        }                      
                    }
                });
                t.start();
                //_________________________________________________fin du chrono
                
                
                
              
                        
                         //---------------------------------------variable de controle
                         //variable permettant d'indiquer si on a charger les questions oui ou non, dans le cas de oui alors on devra afficher les resultats quand on appuie sur arreter
                         //------------------------------------------------------------------------affichage des questions
                        isClicked=true;//on met à true pour dire qu'on a cliqué sur le bouton Démarrer 
                        Connection connection;
                        Statement statement;
                        ResultSet result;
                        ResultSet result1;

                        try {
                                Class.forName("com.mysql.cj.jdbc.Driver").newInstance();          
                                connection=DriverManager.getConnection("jdbc:mysql://localhost/olympiades?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC", "root", "");
                                statement = connection.createStatement();


                                //trouver le nombre d'enregistrement de la table et le stocker dans la variable Max
                                int Min=1, Max=0;
                                //String reqt_nbrEnregis= "SELECT COUNT(DISTINCT nom_colonne) FROM table";        
                                result1 = statement.executeQuery("SELECT COUNT(DISTINCT contenu_question_ntic) AS compte FROM question_ntic;");

                                //tant que y'a un contenu, le ramener
                               while(result1.next())
                               {
                                    Max=result1.getInt("compte");     
                               }

                               //on initialise le tableau à 0 car on est sur qu'aucun nombre généreé ne sera null
                               tabRandom.add(0);

                               //générer un nombre aléatoire 'aleatoire' entre Min et Max et accéder à l'enrengistrement numéro
                               nombreAleatoire = Min + (int)(Math.random() * ((Max - Min) + 1));
                               tabRandom.add(nombreAleatoire);

                                /*int sortir=0;
                               //tant que la question a été posée, on cherche une autre question non encore posée
                               while(tabRandom.contains(nombreAleatoire) && sortir==0)
                               {
                                   //si le nombre existe, on genere un nombre puis on verifie à nouveau si il est déjà dans le tableau
                                    nombreAleatoire = Min + (int)(Math.random() * ((Max - Min) + 1));
                                    if(!(tabRandom.contains(nombreAleatoire)))
                                    {
                                        tabRandom.add(nombreAleatoire);
                                        sortir=1;
                                    }
                               }//on sort avec le bon nombre et on l'insère dans le tableau
                               */

                               String requete="SELECT distinct contenu_question_ntic from question_ntic where id_question_ntic=" +nombreAleatoire;
                               result = statement.executeQuery(requete); 

                                //tant que y'a un contenu, le ramener
                               while(result.next()){
                                question.setText(" "+result.getString("contenu_question_ntic"));      
                                }  
                        } catch (Exception e) {
                           e.printStackTrace();
                        }
                        
                        
                        
                         //________________________________________________affichage des questions en même temps que les réponses

                        {

                            ArrayList<String> lesReponses =new ArrayList<String>(); //tabpaleau dynamique de chaine de caractère qui doit contenir une reponse par indice
                            ArrayList<String> Reponse_valide =new ArrayList<String>();
                            try {
                                    Class.forName("com.mysql.cj.jdbc.Driver").newInstance();          
                                    connection=DriverManager.getConnection("jdbc:mysql://localhost/olympiades?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC", "root", "");
                                    statement = connection.createStatement();

                                         //-----------------on suppose 4 réponses dans l'application, on nettoie le tableau à chaque nouvel apppui
                                        lesReponses.add(0,"0");
                                        lesReponses.add(1,"0");
                                        lesReponses.add(2,"0");
                                        lesReponses.add(3,"0");

                                        Reponse_valide.add(0,"0");
                                        Reponse_valide.add(1,"0");
                                        Reponse_valide.add(2,"0");
                                        Reponse_valide.add(3,"0");    

                                    //table reponse
                                    String requete="SELECT * from reponse_ntic where  id_question_ntic=" +nombreAleatoire;
                                    result = statement.executeQuery(requete); 

                                     //tant que y'a un contenu à lire de la BD, on le lit et on stocke dans le tableau dynamique
                                    int j = 0;
                                   // lesReponses.clear();
                                    //Reponse_valide.clear();
                                    while(result.next())
                                    {     
                                        lesReponses.set(j, result.getString("contenu_reponse_ntic"));
                                        Reponse_valide.set(j,result.getString("bingo_reponse_ntic"));
                                        j++;
                                    }  
                                    //affichage des reponses sur l'interface graphique
                                    reponse0.setText(" "+lesReponses.get(0)+" ");
                                    reponse1.setText(" "+lesReponses.get(1)+" ");
                                    reponse2.setText(" "+lesReponses.get(2)+" ");
                                    reponse3.setText(" "+lesReponses.get(3)+" ");

                                   //recuperer les valeurs pour pouvir mettre les bonne reponsnes en surbriallance quand le chorno finira
                                    bonneReponse0=Reponse_valide.get(0);
                                    bonneReponse1=Reponse_valide.get(1);
                                    bonneReponse2=Reponse_valide.get(2);
                                    bonneReponse3=Reponse_valide.get(3);

                                    lesReponses.clear();
                                    Reponse_valide.clear();

                                    connection.close();
                                    //---------------------------------------------------------------------------------------------------------fin marquage bonne reponse

                                 }
                                catch(Exception e)
                                {
                                    e.printStackTrace();
                                }
                        }//fin affichage des questions                                             
            }//fin if==3 pour les ntic
             //___________________________________________________________________________________________________________________________________________________________________
        
             
             
             
             
             
        
        }//fin de else fin du jeu
        
    }//GEN-LAST:event_bt_runActionPerformed

    
    
    
    
    
    
    int x, y;
    private void jPanel2MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jPanel2MousePressed
        x = evt.getX();
        y = evt.getY();

    }//GEN-LAST:event_jPanel2MousePressed

    private void jPanel2MouseDragged(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jPanel2MouseDragged

        int xx = evt.getXOnScreen();
        int yy = evt.getYOnScreen();
        this.setLocation(xx - x, yy - y);
    }//GEN-LAST:event_jPanel2MouseDragged
    //String saisie1_increment = saisie1.getText();
    //String saisie1_decrement = saisie2.getText();
    public int increment = 5; //=Integer.parseInt(saisie1);
    public int decrement = 5;//Integer.parseInt(saisie2);

    public int score_joueur1 = 0;

    public int score_joueur2 = 0;
     public int score_joueur3 = 0;
    
    private void retrait_joueur1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_retrait_joueur1MouseClicked
        if (score_joueur1 > 0 && (score_joueur1 - decrement) >= 0) {
            score_joueur1 -= decrement;
        }
        scoreA.setText("" + score_joueur1);
    }//GEN-LAST:event_retrait_joueur1MouseClicked

    private void ajout_joueur1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_ajout_joueur1MouseClicked
        score_joueur1 += increment;
        scoreA.setText("" + score_joueur1);
    }//GEN-LAST:event_ajout_joueur1MouseClicked

    private void ajout_joueur2MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_ajout_joueur2MouseClicked
        score_joueur2 += increment;
        scoreB.setText("" + score_joueur2);
    }//GEN-LAST:event_ajout_joueur2MouseClicked

    private void retrait_joueur2MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_retrait_joueur2MouseClicked
        if (score_joueur2 > 0 && (score_joueur2 - decrement) >= 0) {
            score_joueur2 -= decrement;
        }
        scoreB.setText("" + score_joueur2);
    }//GEN-LAST:event_retrait_joueur2MouseClicked

    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
//--------------------------------------------------bouton Arreter

    private void jButton2MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButton2MouseClicked
        //remettre les couleurs par défauts 
        reponse0.setBackground(new Color(237,237,237));
        reponse0.setOpaque(true);
        reponse0.setBorder(BorderFactory.createBevelBorder(1,new Color(237,237,237), new Color(237,237,237), new Color(237,237,237), new Color(237,237,237)));
        reponse0.setForeground(new Color(0,0,0));      

        reponse1.setBackground(new Color(237,237,237));
        reponse1.setOpaque(true);
        reponse1.setBorder(BorderFactory.createBevelBorder(1,new Color(237,237,237), new Color(237,237,237), new Color(237,237,237), new Color(237,237,237)));
        reponse1.setForeground(new Color(0,0,0));

        reponse2.setBackground(new Color(237,237,237));
        reponse2.setOpaque(true);
        reponse2.setBorder(BorderFactory.createBevelBorder(1,new Color(237,237,237), new Color(237,237,237), new Color(237,237,237), new Color(237,237,237)));
        reponse2.setForeground(new Color(0,0,0));

        reponse3.setBackground(new Color(237,237,237));
        reponse3.setOpaque(true);
        reponse3.setBorder(BorderFactory.createBevelBorder(1,new Color(237,237,237), new Color(237,237,237), new Color(237,237,237), new Color(237,237,237)));
        reponse3.setForeground(new Color(0,0,0));
     
        
        //notre panneau des reponses sera visible que quand on cliquera sur le bouton arreter
        reponse_panel.setVisible(true);
        bt_run.setVisible(true);      
        jButton2.setVisible(false);
      

        //arrêter le chrono
        t.stop();
        count=num_;//remettre le compteur à la valeur
        // effacer le label et le remettre à la valeur de num_ jp_progress.setText("");      
    }//GEN-LAST:event_jButton2MouseClicked

    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    private void jLabel21MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel21MouseClicked
        rubrique=0;
        jLabel13.setVisible(true);
        jLabel21.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153,0,0),5));
        jLabel13.setText("Mathématiques");
        
        
         jLabel6.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(204,255,204)));
         jLabel7.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(204,255,204)));
         jLabel23.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(204,255,204)));
         
    }//GEN-LAST:event_jLabel21MouseClicked

    private void jLabel6MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel6MouseClicked
        rubrique=1;
        jLabel13.setVisible(true);
        jLabel6.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153,0,0),5));
        jLabel13.setText("Biologie"); 
        
         jLabel21.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(204,255,204)));
         jLabel7.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(204,255,204)));
         jLabel23.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(204,255,204)));
    }//GEN-LAST:event_jLabel6MouseClicked

    private void jLabel7MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel7MouseClicked
        rubrique=2;
        jLabel13.setVisible(true);
        jLabel7.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153,0,0),5));
        jLabel13.setText("Sciences sociales"); 
        
        jLabel6.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(204,255,204)));
         jLabel21.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(204,255,204)));
         jLabel23.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(204,255,204)));
    }//GEN-LAST:event_jLabel7MouseClicked

    private void jLabel23MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel23MouseClicked
        rubrique=3;
        jLabel13.setVisible(true);
        jLabel23.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153,0,0),5));
        jLabel13.setText("NTICs"); 
        
         jLabel16.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(204,255,204)));
         jLabel7.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(204,255,204)));
         jLabel21.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(204,255,204)));
    }//GEN-LAST:event_jLabel23MouseClicked
    int visible_hide = 1;
    private void jLabel10MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel10MouseClicked
        if(visible_hide == 1){
            jPanel4.setVisible(true);
            visible_hide=0;
        }
        else
        {
           jPanel4.setVisible(false); 
           visible_hide = 1;
           
        }
    }//GEN-LAST:event_jLabel10MouseClicked

    private void jLabel28MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel28MouseClicked
        // TODO add your handling code here:
        Configuration a= new Configuration();
        a.setVisible(true);
        this.hide();
    }//GEN-LAST:event_jLabel28MouseClicked

    private void jLabel17MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel17MouseClicked
            // TODO add your handling code here:
       Authentification a= new Authentification();
       a.setVisible(true);
       this.hide();
    }//GEN-LAST:event_jLabel17MouseClicked

    private void jLabel16MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel16MouseClicked
       Info a= new Info();
       a.setVisible(true);
       this.hide();
    }//GEN-LAST:event_jLabel16MouseClicked

    private void jLabel11MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel11MouseClicked
        this.setState(javax.swing.JFrame.ICONIFIED);
    }//GEN-LAST:event_jLabel11MouseClicked

    private void jLabel12MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel12MouseClicked
        int dialog = JOptionPane.YES_NO_OPTION;
        int result = JOptionPane.showConfirmDialog(null, "Souhaitez-vous vraiment fermer l'application?", "Fermeture de l'application!", dialog);
        if (result == 0) 
        {
            System.exit(0);
        }
    }//GEN-LAST:event_jLabel12MouseClicked

    private void jButton3MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButton3MouseClicked
        //arrêter le chrono
        t.stop();
        count=num_;//remettre le compteur à la valeur
        // effacer le label et le remettre à la valeur de num_ jp_progress.setText("");   
      
        
        //----------------------------------------------------------------------mettre la bonne réponse en surbrillance
        if(bonneReponse0.equals("1"))
        {
            reponse0.setBackground(new Color(153,0,0));
            reponse0.setOpaque(true);
            reponse0.setBorder(BorderFactory.createBevelBorder(1,new Color(0,0,0), new Color(0,0,0), new Color(0,0,0), new Color(0,0,0)));
            reponse0.setForeground(new Color(255,255,255));      
        }

        if(bonneReponse1.equals("1"))
        {
            reponse1.setBackground(new Color(153,0,0));
            reponse1.setOpaque(true);
            reponse1.setBorder(BorderFactory.createBevelBorder(1,new Color(0,0,0), new Color(0,0,0), new Color(0,0,0), new Color(0,0,0)));
            reponse1.setForeground(new Color(255,255,255));       
        }

        if(bonneReponse2.equals("1"))
        {
            reponse2.setBackground(new Color(153,0,0));
            reponse2.setOpaque(true);
            reponse2.setBorder(BorderFactory.createBevelBorder(1,new Color(0,0,0), new Color(0,0,0), new Color(0,0,0), new Color(0,0,0)));
            reponse2.setForeground(new Color(255,255,255));      
        }

        if(bonneReponse3.equals("1"))
        {
            reponse3.setBackground(new Color(153,0,0));
            reponse3.setOpaque(true);
            reponse3.setBorder(BorderFactory.createBevelBorder(1,new Color(0,0,0), new Color(0,0,0), new Color(0,0,0), new Color(0,0,0)));
            reponse3.setForeground(new Color(255,255,255));      
        }
         //----------------------------------------------------------------------fin mettre la bonne réponse en surbrillance
    }//GEN-LAST:event_jButton3MouseClicked

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jButton3ActionPerformed

    //**************************************************************************************************************************************************************
    //******************************************************************************PAS TOUCHE EN BAS***************************************************************
    //**************************************************************************************************************************************************************
    
    private void labelErreurActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_labelErreurActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_labelErreurActionPerformed

    private void jLabel27MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel27MouseExited
        // TODO add your handling code here:
        jLabel27.setBackground(new Color(255,255,255));
        jLabel27.setOpaque(true);
        jLabel27.setForeground(new Color(153,0,0));
        jLabel27.setBorder(BorderFactory.createBevelBorder(1,new Color(153,0,0), new Color(153,0,0), new Color(153,0,0), new Color(153,0,0)));

    }//GEN-LAST:event_jLabel27MouseExited

    private void jLabel27MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel27MouseEntered
        // TODO add your handling code here:
        jLabel27.setBackground(new Color(153,0,0));
        jLabel27.setOpaque(true);
        jLabel27.setForeground(new Color(255,255,255));
        jLabel27.setBorder(BorderFactory.createBevelBorder(1,new Color(255,255,255), new Color(255,255,255), new Color(255,255,255), new Color(255,255,255)));
    }//GEN-LAST:event_jLabel27MouseEntered

    private void jLabel27MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel27MouseClicked

        //--------------------------recuperer les noms des équipes
        String equipeA = labeEquipeA_in.getText();
        String equipeB = labeEquipeB_in.getText();
        String equipeC = labeEquipeC_in.getText();

        labelEquipeA.setText(equipeA.toString());
        labelEquipeB.setText(equipeB.toString());
        labelEquipeC.setText(equipeC.toString());

        //-------------------------------recuperer la minuterie

        try {
            num_ = Integer.parseInt(labelChrono.getText());
            labelErreur.setText("");//effacer le labelErreur si y'a pas d'érreur

        }
        catch (NumberFormatException e)
        {
            // labelErreur.setText("La valeur de la durée doit être un entier!");
        }
    }//GEN-LAST:event_jLabel27MouseClicked

    private void labelChronoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_labelChronoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_labelChronoActionPerformed

    private void labeEquipeA_inActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_labeEquipeA_inActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_labeEquipeA_inActionPerformed

    private void labeEquipeB_inActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_labeEquipeB_inActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_labeEquipeB_inActionPerformed

    private void bt_runMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_bt_runMouseEntered
        // TODO add your handling code here:
        bt_run.setForeground(new Color(0,0,0));
        bt_run.setBorder(BorderFactory.createBevelBorder(1,new Color(0,0,0), new Color(0,0,0), new Color(0,0,0), new Color(0,0,0)));
    }//GEN-LAST:event_bt_runMouseEntered

    private void bt_runMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_bt_runMouseExited
        // TODO add your handling code here:
        bt_run.setForeground(new Color(153,0,0));
        bt_run.setBorder(BorderFactory.createBevelBorder(1,new Color(255,255,255), new Color(255,255,255), new Color(255,255,255), new Color(255,255,255)));
    }//GEN-LAST:event_bt_runMouseExited

    private void jButton2MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButton2MouseEntered
        // TODO add your handling code here:
        jButton2.setForeground(new Color(0,0,0));
        jButton2.setBorder(BorderFactory.createBevelBorder(1,new Color(0,0,0), new Color(0,0,0), new Color(0,0,0), new Color(0,0,0)));
    }//GEN-LAST:event_jButton2MouseEntered

    private void jButton2MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButton2MouseExited
        // TODO add your handling code here:
        jButton2.setForeground(new Color(153,0,0));
        jButton2.setBorder(BorderFactory.createBevelBorder(1,new Color(255,255,255), new Color(255,255,255), new Color(255,255,255), new Color(255,255,255)));
    }//GEN-LAST:event_jButton2MouseExited

    private void jButton3MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButton3MouseExited
        // TODO add your handling code here:
        jButton3.setForeground(new Color(153,0,0));
        jButton3.setBorder(BorderFactory.createBevelBorder(1,new Color(255,255,255), new Color(255,255,255), new Color(255,255,255), new Color(255,255,255)));
    }//GEN-LAST:event_jButton3MouseExited

    private void jButton3MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButton3MouseEntered
        // TODO add your handling code here:
        jButton3.setForeground(new Color(0,0,0));
        jButton3.setBorder(BorderFactory.createBevelBorder(1,new Color(0,0,0), new Color(0,0,0), new Color(0,0,0), new Color(0,0,0)));
    }//GEN-LAST:event_jButton3MouseEntered

    private void ajout_joueur3MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_ajout_joueur3MouseClicked
        // TODO add your handling code here:
        score_joueur3 += increment;
        scoreC.setText("" + score_joueur3);
    }//GEN-LAST:event_ajout_joueur3MouseClicked

    private void retrait_joueur3MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_retrait_joueur3MouseClicked
        // TODO add your handling code here:
        if (score_joueur3 > 0 && (score_joueur3 - decrement) >= 0) {
            score_joueur3 -= decrement;
        }
        scoreC.setText("" + score_joueur3);
    }//GEN-LAST:event_retrait_joueur3MouseClicked

    private void labeEquipeC_inActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_labeEquipeC_inActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_labeEquipeC_inActionPerformed

        
        
        
        
        
        
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel ajout_joueur1;
    private javax.swing.JLabel ajout_joueur2;
    private javax.swing.JLabel ajout_joueur3;
    private javax.swing.JButton bt_run;
    private javax.swing.JPanel corps;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel27;
    private javax.swing.JLabel jLabel28;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JSeparator jSeparator3;
    private javax.swing.JSeparator jSeparator4;
    private olympiades.CustomPanel jp_progress;
    private javax.swing.JTextField labeEquipeA_in;
    private javax.swing.JTextField labeEquipeB_in;
    private javax.swing.JTextField labeEquipeC_in;
    private javax.swing.JTextField labelChrono;
    private javax.swing.JLabel labelEquipeA;
    private javax.swing.JLabel labelEquipeB;
    private javax.swing.JLabel labelEquipeC;
    private javax.swing.JTextField labelErreur;
    private javax.swing.JLabel label_erreur;
    private javax.swing.JPanel menu;
    private javax.swing.JPanel parametre;
    private javax.swing.JPanel principal_bleu;
    private javax.swing.JTextArea question;
    private javax.swing.JLabel reponse0;
    private javax.swing.JLabel reponse1;
    private javax.swing.JLabel reponse2;
    private javax.swing.JLabel reponse3;
    private javax.swing.JPanel reponse_panel;
    private javax.swing.JLabel retrait_joueur1;
    private javax.swing.JLabel retrait_joueur2;
    private javax.swing.JLabel retrait_joueur3;
    private javax.swing.JLabel scoreA;
    private javax.swing.JLabel scoreB;
    private javax.swing.JLabel scoreC;
    // End of variables declaration//GEN-END:variables

    
    @Override
    public void OnProgressComplete() 
    {    
        new Thread(new Runnable() {
            @Override
            public void run() 
            {
                 bt_run.setVisible(true);    
            }
        }).start();
    }
}
