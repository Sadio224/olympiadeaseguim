/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package olympiades;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.RenderingHints;
import java.awt.geom.Arc2D;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Rectangle2D;
import javax.swing.JPanel;

/**
 *
 * @author hp
 */
public class CustomPanel extends JPanel
{
    int progress=60; 
    ProgressBarInterface pbr;
    public void UpdateProgress(int progress_value)
    {
        progress= progress_value;
        if(progress==60)
        {
            pbr.OnProgressComplete();
        }
    }
    
    
    
    @Override
    public void paint(Graphics g){
        super.paint(g);
        Graphics2D g2=(Graphics2D)g;
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g2.translate(this.getWidth()/2, this.getHeight()/2);
        g2.rotate(Math.toRadians(270));
        Arc2D.Float arc=new Arc2D.Float(Arc2D.PIE);
        Ellipse2D circle= new Ellipse2D.Float(0, 0, 110, 110);
        arc.setFrameFromCenter(new Point(0,0), new Point(70,70));
        circle.setFrameFromCenter(new Point(0,0), new Point(65,65));
        arc.setAngleStart(0);
        arc.setAngleExtent(-progress*6.0); //c'est ici q'uon regle le progressbar, le remplissage de l'ellipse
        g2.setColor(Color.red);
        g2.draw(arc);
        g2.fill(arc);
        g2.setColor(Color.WHITE);
        g2.draw(circle);
        g2.fill(circle);
        g2.setColor(Color.red);
        
        g2.rotate(Math.toRadians(90));//--------------------------- rotation du minuterie
        g.setFont(new Font("Verdana", Font.PLAIN, 60));
        FontMetrics fm=g2.getFontMetrics();
        Rectangle2D r=fm.getStringBounds(progress+"", g);
        int x=(0-(int)r.getWidth())/2;
        int y=(0-(int)r.getHeight())/2+fm.getAscent();
        
        g2.drawString(progress+"",x,y);     
    }

    void setText(String count) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    
    public interface ProgressBarInterface{
        public void OnProgressComplete();
        
    }
}
